import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { environment } from 'src/environments/environment';
import {GoogleLoginProvider } from "angularx-social-login";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  //Declarationn des variables
  auth: boolean = false;
  private SERVER_URL:String = environment.SERVER_URL;
  public user:SocialUser|undefined;
  authState$:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.auth);
  userData$:BehaviorSubject<any>= new BehaviorSubject<any>(null);


  constructor(private authservices: SocialAuthService,
              private http: HttpClient
              ) {
                authservices.authState.subscribe(
                  (user:SocialUser)=>{
                    if(user != null){
                      this.auth = true;
                      this.authState$.next(this.auth);
                      this.userData$.next(user);
                    }
                  }
                );
               }
               //User login with email and password

              //  loginUser(email:String,pass:String){
              //    this.http.post(`${this.SERVER_URL}/auth/login`,{email,pass})
              //    .subscribe(
              //      (data:ResponseModel)  =>{
              //        this.auth = data.auth;
              //        this.authState$.next(this.auth);
              //        this.userData$.next(data);
              //      });
              // }

              // Google Authentification

              signInWithGoogle(): void {
                this.authservices.signIn(GoogleLoginProvider.PROVIDER_ID)
                .then(
                  (data)=>{
                    localStorage.setItem('google_auth',JSON.stringify(data));
                  }
                )
              }

              signOut(): void {
                this.authservices.signOut();
              }
              refreshToken(): void {
                this.authservices.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
              }

}
 export interface ResponseModel{

  // let email = req.body.email;
  // let username = email.split("@")[0];
  // let password = await bcrypt.hash(req.body.password, 10);
  // let fname = req.body.fname;
  // let lname = req.body.lname;
  token:String;
  auth:boolean;
  email:String;
  username:String;
  fname:String;
  lname:String;
  photoUrl:String;
  userId:number;


}
