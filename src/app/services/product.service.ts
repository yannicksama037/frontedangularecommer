import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductModelServer, serverResponse} from "../models/product.model";
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = environment.SERVER_URL;

  constructor(private http: HttpClient) { }
  //this is to fetch all product from backend server
  getAllProducts(limitOfResults=10): Observable<serverResponse> {
    return this.http.get<serverResponse>(this.url + '/products', {
      params: {
        limit: limitOfResults.toString()
      }
    });
  }

  //Get a single product from the server
  getSingleProduct(id:number): Observable<ProductModelServer> {
    return this.http.get<ProductModelServer>(this.url + '/products/' + id)
  }

  //Get product from one category
  getProductsFromCategory(catName:string): Observable<ProductModelServer[]>{
    return this.http.get<ProductModelServer[]>(this.url + '/products/category/' +catName);
  }

}
