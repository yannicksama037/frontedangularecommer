import { CartService } from './../../services/cart.service';
import { CartModelServer } from './../../models/cart.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartData!: CartModelServer;
  cartTotal!:number;
  subTotal!: number;

  constructor(public cartservice: CartService) { }

  ngOnInit(): void {
    this.cartservice.cartDataObs$.subscribe(data => this.cartData = data);
    this.cartservice.cartTotal$.subscribe(total => this.cartTotal = total);
  }

  changeQuantity(index:number, increase:boolean){
    this.cartservice.UpdateCartData(index, increase);

  }

}
