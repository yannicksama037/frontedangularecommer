import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public userDetails: any;

  constructor( private authservices:SocialAuthService,
               public userservices:UserService,
               private router:Router) { }

  ngOnInit(): void {
    // this.userservices.userData$
    // .pipe(
    //   map(
    //     (user: SocialUser) =>{
    //       if(user instanceof SocialUser){
    //         return{
    //           email:'yannick-sama037.gmail.com',
    //           password:'root'
    //         }
    //       }else{
    //         return user;
    //       }
    //     }
    //   )
    // )
    // .subscribe(
    //   data =>{
    //     this.myUser=data;
    //   }
    // )

    const storage = localStorage.getItem('google_auth');
    if(storage){
      this.userDetails = JSON.parse(storage)
    }else{
      this.signOut();
    }
  }
  // signOut(): void {
  //   this.userservices.signOut();
  //   this.router.navigateByUrl('/login').then();
  // }

  signOut(): void {
    this.authservices.signOut()
    this.router.navigateByUrl('/login').then();
    // this.authservices.signOut();
  }

}
