import { CartService } from './../../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import {ProductModelServer, serverResponse} from '../../models/product.model'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products: any[] = [];

  constructor(private productservice: ProductService,
              private cartservice: CartService,
              private router : Router) { }

  ngOnInit(): void {
    this.productservice.getAllProducts(12).subscribe((prods: serverResponse ) => {
      this.products = prods.products;
      console.log(this.products);
    });
  }

  selectProduct(id: number){
    this.router.navigate(['/product', id]).then();
  }

  AddToCart(id:number){
    this.cartservice.AddProductToCart(id);

  }


}
