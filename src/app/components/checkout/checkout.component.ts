import { OrderService } from './../../services/order.service';
import { CartService } from './../../services/cart.service';
import { CartModelServer } from './../../models/cart.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  cartTotal!: number;
  cartData!: CartModelServer;

  constructor(private cartservice: CartService,
              private orderservice: OrderService,
              private router : Router,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.cartservice.cartTotal$.subscribe(total => this.cartTotal = total);
    this.cartservice.cartDataObs$.subscribe(data => this.cartData = data);
  }

  onCheckout(){
    this.spinner.show().then(p =>{
      this.cartservice.CheckoutFromCart(2);
    })
  }

}
