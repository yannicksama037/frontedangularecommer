import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email:String|undefined;
  password:String|undefined

  constructor(private authservice:SocialAuthService,
              private router:Router,
              private route:ActivatedRoute,
              private userservice:UserService) { }

  ngOnInit(): void {
    this.userservice.authState$
    .subscribe(
      authState =>{
        if(authState){
          this.router.navigateByUrl(this.route.snapshot.queryParams['returnUrl'] || '/profile')
        }else{
          this.router.navigateByUrl('/login')
        }
      }
    )
  }
  signInWithGoogle(): void{
    this.userservice.signInWithGoogle();
    this.router.navigateByUrl('/profile').then();
  }


}
