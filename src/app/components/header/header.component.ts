import { UserService } from './../../services/user.service';
import { CartService } from './../../services/cart.service';
import { CartModelServer } from './../../models/cart.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cartData!: CartModelServer;
  cartTotal!: number;
  authstate!:boolean;

  constructor(public cartservice: CartService,private userservice:UserService) { }

  ngOnInit(): void {
    this.cartservice.cartTotal$.subscribe(total => this.cartTotal = total);
    this.cartservice.cartDataObs$.subscribe(data => this.cartData = data);
    this.userservice.authState$.subscribe(
      (authstate)=>{
        this.authstate = authstate;
      }
    )
  }

}
